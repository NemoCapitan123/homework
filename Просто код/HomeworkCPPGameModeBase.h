// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RunnableThreads/RunnableRoundWord.h"
#include "HAL/RunnableThread.h"
#include "HomeworkCPPGameModeBase.generated.h"
/**
 * 
 */
UCLASS()
class HOMEWORKCPP_API AHomeworkCPPGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:
	AHomeworkCPPGameModeBase();
	~AHomeworkCPPGameModeBase();

	virtual void BeginPlay() override;
	class RunnableRoundWord* WordGenerator = nullptr; //��������� ����� ���������� ����
	FRunnableThread* ThreadWord = nullptr; //������ ����� ������
};
