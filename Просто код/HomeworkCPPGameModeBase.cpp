// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.


#include "HomeworkCPPGameModeBase.h"

AHomeworkCPPGameModeBase::AHomeworkCPPGameModeBase()
{
}

AHomeworkCPPGameModeBase::~AHomeworkCPPGameModeBase()
{
}

void AHomeworkCPPGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	if (ThreadWord == nullptr) {
		if (WordGenerator == nullptr) {
		WordGenerator = new RunnableRoundWord();
		}
		ThreadWord = FRunnableThread::Create(WordGenerator, TEXT("GenerateWords"), 0, EThreadPriority::TPri_Normal);
	}
}
